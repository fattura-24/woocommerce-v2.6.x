<?php

/*
 * Plugin Name: Fattura24
 *
 * Plugin URI: http://www.fattura24.com
 * Description: Create your invoices with Fattura24.com
 *
 * License: EULA - @Fattura24.com
 * Version: 2.6.13
 *
 * Text Domain: fatt-24
 * Domain Path: /languages
 *
 * Author: Fattura24.com
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'src/behaviour.php';

if (is_admin()) {

    require_once 'src/uty.php';
    require_once 'src/hooks.php';
    require_once 'src/settings.php';

    // code executed only on admin side
    add_action('admin_menu', function() {
        add_options_page(
            __('Settings Fattura 24', 'fatt-24'), 'Fattura24',
            'manage_options',
            SETTINGS_PAGE,
            __NAMESPACE__.'\show_settings');
    });
    add_action('admin_init', __NAMESPACE__.'\init_settings');

    // define a root
    function webase($page) { return 'http://www.fattura24.com/'.$page; }

    function page($topic = null, $ico = 'logo_orange') {
        echo div(array($topic ? h1($topic) : ''
                , img(attr('src', png('../images/'.$ico)), array())
                , div(style(array('float'=>'right', 'padding'=>'20px')), array(
                        a(array('href'=>webase('')), __('Support', 'fatt-24')), '|',
                        a(array('href'=>webase('woocommerce-plugin-fatturazione')), __('Documentation', 'fatt-24'))))
        ));
    }

    // this plugin works only with WooCommerce installed
    //  display a notice if this is not the case
    add_action('admin_notices', function() {
        if (!class_exists('WooCommerce'))
            echo div(klass('updated'), p(__('Fattura 24 plugin requires WooCommerce installed', 'fatt-24')));
    });

    // AJAX call to check API key
    add_action('wp_ajax_api_verification', function() {
        check_ajax_referer(FATT_24_API_VERIF_NONCE, 'security');
        wp_send_json(verifica_api_key($_POST['api_key']));
    });
    
    // factorize out PDF document JS control function
    add_action('admin_enqueue_scripts', function() {
        $js = 'src/f24_pdfcmd.js';
        wp_enqueue_script('f24_pdfcmd', plugins_url($js, __FILE__), array('jquery'));
    });
    
    register_activation_hook(__FILE__, function() {
        $rc = move_docs(DOCS_FOLDER);
        trace('move_docs', $rc);
    });
}
else {
    require_once 'src/hooks.php';
}

add_action('plugins_loaded', function() {
    $rc = load_plugin_textdomain('fatt-24', false, dirname(plugin_basename(__FILE__)).'/languages');
});
