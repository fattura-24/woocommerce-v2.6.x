# Changelog

## 2.6.13
###### _Nov 16, 2017_ 
- aggiunta selezione sezionale per ricevute e fatture nelle impostazioni del modulo. Ora è possibile scegliere il sezionale dei documenti creati dagli e-commerce, cosicché si possono usare sezionali diversi per diversi e-commerce

## 2.6.12 
###### _Nov 10, 2017_ 
- aggiunta selezione modello PDF per ordini e fatture con destinazio neelle impostazioni del modulo. Ora è possibile scegliere modelli diversi per i documenti a seconda che l'ordine contenga o meno l'indirizzo di spedizione del cliente
- aggiunta selezione conto nelle impostazioni del modulo. Ora è possibile scegliere il conto economico da associare alle prestazioni/prodotto dei documenti generati dall'e-commerce

## 2.6.11
###### _Ott 12, 2017_
- risolto bug su creazione ordine da parte di un utente non registrato

## 2.6.10
###### _Set 12, 2017_
- aggiunti alcuni messaggi di avviso nella pagina di configurazione del modulo
- rimossa checkbox 'Scarica PDF' relativa ai PDF delle fatture nella pagina di configurazione del modulo. Ora i PDF vengono sempre scaricati sul proprio e-commerce quando viene creata una fattura

## 2.6.9
###### _Set 01, 2017_
- risolto bug che causava un errore durante l'installazione se la versione di PHP è minore della 5.5

## 2.6.8
###### _Ago 24, 2017_
- spostati i campi 'Codice Fiscale' e 'Partita IVA' nella pagina di Checkout dell'ordine. Ora sono nella sezione 'Dettagli di fatturazione'

## 2.6.7
###### _Ago 02, 2017_
- aggiunta gestione magazzino nella pagina di configurazione del modulo. Ora è possibile decidere se gli ordini e/o le fatture movimentano il magazzino in Fattura24
- aggiunta configurazione per i modelli di ordini e fatture. Ora è possibile scegliere con quale modello vengono creati i PDF degli ordini e delle fatture

## 2.6.6
###### _Giu 28, 2017_
- aggiunta funzionalità per cui l'ordine impegna la merce nel magazzino di Fattura24 e la fattura la scarica se il prodotto ha lo stesso codice che ha sull'e-commerce
- ora i dati dell'ordine vengono inviati a Fattura24 anche se questo viene creato, o editato (se i suoi dati non sono mai stati inviati precedentemente), dal pannello di amministrazione

## 2.6.5
###### _Giu 20, 2017_
- aggiunto avviso ‘Nuova versione disponibile’ nella pagina di configurazione del plugin
- risolto bug minore

## 2.6.4
###### _Giu 12, 2017_
- risolto bug minore

## 2.6.3
###### _Giu 5, 2017_
- risolto bug minore

## 2.6.2
###### _Mag 24, 2017_
- risolto bug per cui non veniva creato l’ordine in Fattura24 quando il pagamento era effettuato tramite Paypal

## 2.6.1
###### _Mag 17, 2017_
- risolto bug su invio Iva errata

## 2.6.0
###### _Mag 16, 2017_
- risolto bug che si verificava in presenza di prodotti scontati tramite certi plugin di WordPress
- risolti bug minori

## 1.4.5
###### _Mar 29, 2017_
- la creazione della fattura è stata vincolata al passaggio dell’ordine nello stato definitivo.
- introdotta la piena compatibilità con PHP7
- risolti bug minori

## 1.4.4
###### _Mar 15, 2017_
- abilitato il passaggio dei dati relativi alla destinazione della merce
- abilitata la configurazione per disabilitare la creazione delle ricevute
- risolti bug minori

## 1.4.1
###### _Feb 27, 2017_
- risolti i problemi che si verificavano in presenza di sconti carrello e prodotto

## 1.4.0
###### _Feb 2, 2017_
- aggiunta la funzione per scaricare da Fattura24 il PDF di un documento creato dall’eCommerce ma modificato in Fattura24
creazione del PDF dell’ordine utilizzando il numero d’ordine presente sull’eCommerce
- aggiunta l’opzione di configurazione che permette di non creare l’ordine o la fattura ma di salvare solo il contatto del cliente nella rubrica
- aggiunta la gestione dinamica delle modalità di pagamento direttamente dall’eCommerce
- aggiunta l’opzione di configurazione che permette di indicare il C.F. e/o la P.IVA come campi obbligatori per concludere l’acquisto
- corretti bug minori

## 1.3.x
- aggiunta la possibilità di creare su Fattura24 anche il PDF dell’ordine
- aggiunta l’opzione di configurazione per inviare il PDF dell’ordine e della fattura direttamente al cliente via email
- aggiunto pulsante per verificare la correttezza dell’API KEY