<?php

/**
 * Fattura24.com
 * Description: invoicing API could fail for a number of reasons, handle with post metadata
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'constants.php';
require_once 'uty.php';

/* from https://docs.woocommerce.com/document/managing-orders/
    Pending payment – Order received (unpaid)
    Failed – Payment failed or was declined (unpaid). Note that this status may not show immediately and instead show as pending until verified (i.e., PayPal)
    Processing – Payment received and stock has been reduced- the order is awaiting fulfillment.
    Completed – Order fulfilled and complete – requires no further action
    On-Hold – Awaiting payment – stock is reduced, but you need to confirm payment
    Cancelled – Cancelled by an admin or the customer – no further action required
    Refunded – Refunded by an admin – no further action required
*/

// on large wp_postmeta, the update/reload can fail without notice...
// implement a trivial caching system by means of this global
$last_order_status = array();

// these are relatives to Fattura24 transmitted data
//  normally *should* result in a locally downloaded PDF
function store_order_status($order_id, $status) {
    global $last_order_status;
    $rc = update_post_meta($order_id, ORDER_INVOICE_STATUS, $status);
    //trace('store_order_status', $order_id, $status, $rc);
    $last_order_status[$order_id] = $status;
}
function get_order_status($order_id) {
    global $last_order_status;
    if (isset($last_order_status[$order_id]))
        return $last_order_status[$order_id];
    $retv = get_post_meta($order_id, ORDER_INVOICE_STATUS, true);
    return $retv;
}

function order_status_add_fields(&$status, $fields) {
    $h_save = array();
    foreach($fields as $k => $v) {
        if (isset($status[$k])) // && $status[$k] != $v)
            $h_save[$k] = $status[$k];
        $status[$k] = $v;
    }
    if (!empty($h_save)) {
        if (isset($status['history']))
            $h_save['history'] = $status['history'];
        $status['history'] = $h_save;
    }
}

function order_status_set_file_data(&$status, $pdfPath, $docType) {
    //order_status_add_fields($status, compact('pdfPath', 'docType'));
    if ($status['docType'] == $docType)
        $status['pdfPath'] = $pdfPath;
    else if (isset($status['history']))
        order_status_set_file_data($status['history'], $pdfPath, $docType);
}
function order_status_set_doc_data(&$status, $returnCode, $description, $docId, $docType) {
    order_status_add_fields($status, compact('returnCode', 'description', 'docId', 'docType'));
}
function order_status_set_error(&$status, $error) {
    order_status_add_fields($status, array('lastErr' => now().' : '.$error));
}

function order_status($order_id) {
    $rec = get_order_status($order_id);

    $status = INVSTA_NONE;
    $info = null;
    
    while ($rec) {
        if (isset($rec['pdfPath']) && peek($rec, 'docType') == DT_FATTURA) {
            $status = INVSTA_PDF_AVAIL_LOCAL;
            $info = $rec;
            break;
        }
        if (peek($rec, 'docId') && peek($rec, 'docType') == DT_FATTURA) {
            $status = INVSTA_PDF_AVAIL_SERVER;
            $info = $rec;
            break;
        }
        $rec = peek($rec, 'history');
    }

    $retv = array(
        'status'    =>$status,
        'info'      =>$info
    );
    return $retv;
}

add_action('wp_ajax_invoice_admin_command', function() {
    $args = $_POST['args'];
    //trace('wp_ajax_invoice_admin_command', $args);

    $order_id = $args['id'];
    $doc_type = $args['type'];
    check_ajax_referer(FATT_24_ORDER_ACTIONS_NONCE, 'security');
    
    $s = null;
    if ($args['cmd'] == 'upload') {
        if (store_fattura24_doc($order_id, $doc_type))
        {
            download_PDF($order_id);
            wp_send_json(array(1, actions_of_order($order_id, $doc_type, true)));
        }
    }
    else {  // 'update'
        if ($s = get_order_status($order_id)) {
            if (download_PDF($order_id))
                wp_send_json(array(1, actions_of_order($order_id, $doc_type, true)));
        }
    }
    if (!$s)
        $s = get_order_status($order_id);
        
    $err = null;
    if ($s) {
        if (isset($s['lastErr']))
            $err = $s['lastErr'];
        else if (isset($s['description']) && isset($s['returnCode']))
            $err = $s['description'];
    }

    wp_send_json(array(0, $err));
});

function admin_actions($status) {

    $r = array(
        array('label' => __('Create Invoice','fatt-24'),  'cmd' => 'upload',   'enabled' => false),
        array('label' => __('View PDF','fatt-24'),        'cmd' => 'view',     'enabled' => false),
        array('label' => __('Update PDF','fatt-24'),      'cmd' => 'update', 'enabled' => false)
    );

    switch ($status) {
    case INVSTA_PDF_AVAIL_LOCAL:
        $r[1]['enabled'] = // enable downloading again
        $r[2]['enabled'] = true;
        break;
    case INVSTA_PDF_AVAIL_SERVER:
        $r[1]['enabled'] = true;
        break;
    default:
        $r[0]['enabled'] = true;
        break;
    }
    return $r;
}

function order_PDF_url($orderId) {
    $st = get_order_status($orderId);
    if ($st && isset($st['pdfPath']))
        return get_url_from_file($st['pdfPath']);
}

function peek($v, $k, $default = null) { return isset($v[$k]) ? $v[$k] : $default; }

/*
 * Display link to invoice PDF on the order edit page, or appropriate action
 *  note that actually the only doc_type allowed by now is DT_FATTURA
 */
function actions_of_order($order_id, $doc_type, $only_inner = false)
{
    $s = order_status($order_id);
    //trace('actions_of_order', $order_id, $doc_type, $only_inner, $s);

    $r = admin_actions($s['status']);
    $wp_nonce = wp_create_nonce(FATT_24_ORDER_ACTIONS_NONCE);
    
    $cmd = function($r, $id) use ($doc_type, $wp_nonce) {
        $cmd = $r['cmd'];
        $onclick = "f24_pdfcmd('$id','$cmd','$doc_type','$wp_nonce')";
        if ($r['enabled'])
            return a(array('href'=>'#', 'onclick'=>$onclick), $r['label']);
        return $r['label'];
    };
    $pdf = function($r, $order_id) {
        if ($r['enabled'])
            return a(array('href'=>order_PDF_url($order_id), 'target'=>'_blank'), $r['label']);
        return $r['label'];
    };
    
    $h = array(
        $cmd($r[0], $order_id),
        $pdf($r[1], $order_id),
        $cmd($r[2], $order_id)
    );
    if ($only_inner)
        $html = implode('<br>', $h);
    else
        $html = div(id('cmds-'.$order_id), implode('<br>', $h));

    return $html;
}

/* group actions depending on selected flags
 * required to avoid duplicated invoices when status goes to 'wc-completed' multiple times
 */
function process_fattura($order_id)
{
    if (get_flag(INV_CREATE))
    {
        //trace('in process_fattura');
        if (!is_available_on_f24($order_id, DT_FATTURA))
            store_fattura24_doc($order_id, DT_FATTURA);
        //trace('out process_fattura');
    }
}
function process_order($order_id)
{
    if (get_flag(ORD_CREATE))
    {
        //trace('in process_order');
        if (!is_available_on_f24($order_id, DT_ORDINE))
            store_fattura24_doc($order_id, DT_ORDINE);
        //trace('out process_order');
    }
}
function process_customer($order_id)
{
    if (get_flag(ABK_SAVE_CUST_DATA) && !get_flag(ORD_CREATE) && !get_flag(INV_CREATE))
    {
        //trace('in process_customer');
        SaveCustomer($order_id);
        //trace('out process_customer');
    }
}

// get docId stored on F24
function is_available_on_f24($order_id, $docType)
{
    $s = get_order_status($order_id);
    //trace('is_available_on_f24', $order_id, $docType, $s);
    while ($s)
    {
        if (peek($s, 'docType') == $docType)
        {
            if ($docId = peek($s, 'docId'))
            {
                trace('docId found', $docId);
                return $docId;
            }
        }
        $s = peek($s, 'history');
    }
    //trace('nope');
    return null;
}
function is_PDF_available($order_id, $docType)
{
    $s = get_order_status($order_id);
    //trace('is_PDF_available', $order_id, $docType, $s);
    while ($s)
    {
        if (peek($s, 'docType') == $docType && peek($s, 'docId'))
        {
            if ($pdfPath = peek($s, 'pdfPath'))
                if (is_file($pdfPath))
                    return $pdfPath;
        }
        $s = peek($s, 'history');
    }
    //trace('nope');
    return null;
}
