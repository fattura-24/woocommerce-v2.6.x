<?php

/**
 * Fattura24.com
 * Description: define symbols used in this plugin
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

define('SETTINGS_PAGE',         'fatt-24-settings');
define('SETTINGS_GROUP',        'fatt-24-group');

define('OPT_SECT_ID',           'fatt-24-API-sect-id');
define('OPT_API_KEY',           'fatt-24-API-key');
define('OPT_API_VERIFICATION',  'fatt-24-API-verification');
define('OPT_PLUGIN_VERSION',    'fatt-24-API-plugin-version');

// perform document' location update once
// with default values at installation time
define('DOCS_FOLDER',           'fattura24/pdf');

define('ABK_SECT_ID',           'fatt-24-addrbook-sect-_id');
define('ABK_SAVE_CUST_DATA',    'fatt-24-abk-save-cust-data');
define('ABK_FISCODE_REQ',       'fatt-24-abk-fiscode-req');
define('ABK_VATCODE_REQ',       'fatt-24-abk-vatcode-req');

define('ORD_SECT_ID',           'fatt-24-ord-sect-id');
define('ORD_CREATE',            'fatt-24-ord-enable-create');
define('ORD_DOWNLOAD',          'fatt-24-ord-download-pdf');
define('ORD_SEND',              'fatt-24-ord-send-pdf');
define('ORD_STOCK',             'fatt-24-ord-stock');
define('ORD_TEMPLATE',          'fatt-24-ord-template');
define('ORD_TEMPLATE_DEST',     'fatt-24-ord-template-dest');

define('INV_SECT_ID',           'fatt-24-inv-sect-id');
define('INV_CREATE',            'fatt-24-inv-create');
define('INV_DOWNLOAD',          'fatt-24-inv-download-pdf');
define('INV_SEND',              'fatt-24-inv-send-pdf');
define('INV_STOCK',             'fatt-24-inv-stock');
define('INV_WHEN_PAYED',        'fatt-24-inv-create-when-payed');
define('INV_DISABLE_RECEIPTS',  'fatt-24-inv-disable-receipts');
define('INV_TEMPLATE',          'fatt-24-inv-template');
define('INV_TEMPLATE_DEST',     'fatt-24-inv-template-dest');
define('INV_PDC',               'fatt-24-inv-pdc');
define('INV_SEZIONALE_RICEVUTA','fatt-24-inv-sezionale-ricevuta');
define('INV_SEZIONALE_FATTURA', 'fatt-24-inv-sezionale-fattura');

// define API call URL
define('API_ROOT', 'https://www.app.fattura24.com/api/v0.3');

// metakey to store serialized status data
// (contains docId as well as API response details)
define('ORDER_INVOICE_STATUS',  'fatt-24-order-invoice-status');

// qualify actions and outcomes performed to API
define('INVSTA_NONE', 0);
//define('INVSTA_STORED', 1);
define('INVSTA_STORED_FAILED', 2);
define('INVSTA_PDF_AVAIL_LOCAL', 3);
define('INVSTA_PDF_AVAIL_SERVER', 4);

// there are different document types associated to a single order
// these constants match the values required on Fattura24 API
// FATTURA e RICEVUTA are exclusive (same document),
// depend on availability of customer P.IVA
define('DT_FATTURA',    'I');
define('DT_RICEVUTA',   'R');
define('DT_ORDINE',     'C');
define('DT_FATTURA_FORCED', 'I-force');

// num. of items abbrev.
define('PRODUCT_XML_UM', 'pz');

// filters names to refine plugin behaviour
define('DOC_PDF_FILENAME',          'fatt-24-doc-pdf-filename');

define('CUSTOMER_USER_DATA',        'fatt-24-customer-user-data');


define('CUSTOMER_USE_VAT',          'fatt-24-customer-use-vat');
define('CUSTOMER_USE_CF',           'fatt-24-customer-use-cf');
define('ORDER_GET_VAT',             'fatt-24-order-get-vat');
define('ORDER_GET_CF',              'fatt-24-order-use-cf');

define('DOC_STORE_FILE',            'fatt-24-doc-store-file');

define('PRODUCT_ITEM_DESC',         'fatt-24-product-item-desc');
define('PRODUCT_ITEM_DISCOUNT',     'fatt-24-product-item-discount');

define('DOC_FOOTNOTES',             'fatt-24-doc-footnotes');
define('DOC_OBJECT',                'fatt-24-doc-object');
define('DOC_PRODUCT_CODE',          'fatt-24-doc-product_code');
define('DOC_ADDRESS',               'fatt-24-doc-address');
define('DOC_SHIPPING_CALC',         'fatt-24-doc-shipping-calc');

define('ON_STATUS_CHANGE',          'fatt-24-on-status-change');
define('ON_THANK_YOU',              'fatt-24-on-thank-you');

define('COUPON_DESCRIPTION',        'fatt-24-coupon-description');

// returns HTML to echo for settings page controls
define('LAYOUT_OPTION',             'fatt-24-layout-option');

// API fields hardcoded max values length - from email 2/11/2016
define('API_FIELD_MAX_indirizzo',   100);
define('API_FIELD_MAX_citta',       50);
define('API_FIELD_MAX_provincia',   2);
define('API_FIELD_MAX_cap',         10);
define('API_FIELD_MAX_paese',       50);

// nonces for AJAX security
define('FATT_24_API_VERIF_NONCE',       '@ fattura24 @ API verification');
define('FATT_24_ORDER_ACTIONS_NONCE',   '@ fattura24 @ order actions');
