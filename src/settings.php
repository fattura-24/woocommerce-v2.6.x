<?php

/**
 * Fattura24.com
 * Description: handle basic settings
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH'))
    exit;

require_once 'settings_uty.php';
require_once 'api_call.php';

if (is_admin())
{
    function plugin_version()
    {
        if ($plugin_data = get_plugin_data(plugin_dir_path(__FILE__).'../fattura24.php'))
            return $plugin_data['Version'];
        throw new \Exception(__('no plugin information available', 'fatt-24'));
    }
    
    function woocommerce_version_check()
    {
        if(class_exists('WooCommerce'))
        {
            global $woocommerce;
            if(version_compare($woocommerce->version, '3.0.0', "<" ))
                return true;
            else
                return false;
	    }
	    return true;
    }
    
    function url_exists($url)
    {
        return strpos(@get_headers($url)[0],'200') === false ? false : true;
    }

    function getVersionCheckMessage()
    {
        $datePlugin = '2017/11/16 11:00';
        $versionCheckMessage = '';
        $urlCheckVersion = "https://www.fattura24.com/woocommerce/latest_version_woo_2.txt";
        if(url_exists($urlCheckVersion))
        {
            $fileCheckVersion = fopen($urlCheckVersion, "r");
            $textCheckVersion = stream_get_contents($fileCheckVersion);
            $latest_version_date = substr($textCheckVersion,0,16);
            $urlDownload = substr($textCheckVersion,17);
            fclose($fileCheckVersion);
        }
        if(woocommerce_version_check())
        {
            if(url_exists($urlCheckVersion) && $latest_version_date != $datePlugin)
            {
                $versionCheckMessage .= getNoticeHtml('È stata rilasciata una nuova versione del modulo! Clicca <a href="' 
                    . $urlDownload . '" target="_blank">qui</a> per scaricarla');
            }
        }
        else
            $versionCheckMessage .= getErrorHtml('Attenzione: questo modulo non è compatibile con la corrente versione di Magento. 
                Clicca <a href="https://www.fattura24.com/woocommerce-plugin-fatturazione" target="_blank">qui</a> per scaricare la versione corretta del modulo');

        return $versionCheckMessage;
    }

    function getApiKeyCheckMessage()
    {
        $apiKeyCheck = testApiKey();
        $apiKey = get_option("fatt-24-API-key");
        $apiKeyMessage = '';
        if($apiKeyCheck['returnCode'] == 1)
            $apiKeyMessage .= '<span style="color:green;">&nbsp;&nbsp;Api Key verificata</span>';
        else if(empty($apiKey))
            $apiKeyMessage .= getErrorHtml('Api Key non inserita');
        else if(!empty($apiKey))
            $apiKeyMessage .= getErrorHtml($apiKeyCheck['description']);
        if(!$apiKeyCheck['subscriptionTypeIsValid'])
            $apiKeyMessage .= getErrorHtml('Attenzione: per utilizzare questo servizio devi avere un abbonamento Business');
        $subscriptionDaysToExpiration = $apiKeyCheck['subscriptionDaysToExpiration']; 
        if($subscriptionDaysToExpiration < 31)
            $apiKeyMessage .= getNoticeHtml('Mancano ' . $subscriptionDaysToExpiration . ' giorni alla scadenza del tuo abbonamento');
        
        return $apiKeyMessage;
    }

    function getNoticeHtml($message)
    {
        return '<div id="setting-error-settings_updated" class="notice notice-warning"> 
            <p><strong>' . $message . '</strong></p></div>';
    }

    function getErrorHtml($message)
    {
        return '<div id="setting-error-settings_updated" class="notice notice-error"> 
            <p><strong>' . $message . '</strong></p></div>';
    }
}

// create multiple setting sections, to enter the API key and further configuration
function init_settings()
{
    $sect_key = array(
        'section_header'    =>__('General Settings','fatt-24'),
        'section_callback'  =>null,
        'section_id'        =>OPT_SECT_ID,
        'fields'            =>array(
            OPT_PLUGIN_VERSION=>array(
                'type'  => 'label',
                'label' => __('Plugin Version','fatt-24'),
                'text'  => plugin_version(),
                'desc' => getVersionCheckMessage()
            ),

            OPT_API_KEY=>array(
                'type'  => 'text',
                'size'  => '40',
                'label' => __('Api Key', 'fatt-24'),
                'desc' => getApiKeyCheckMessage()
            )
        )
    );

    /* Sezione Rubrica */
    $sect_addrbook = array(
        'section_header'    => '<hr/>'.__('Address Book','fatt-24'),
        'section_callback'  => null,
        'section_id'        => ABK_SECT_ID,
        'fields'            => array(
            ABK_SAVE_CUST_DATA=>array(
                'type'  => 'bool',
                'default' => true,
                'label' => __('Save Customer','fatt-24'),
                'desc'  => __(' Enable saving Customer data on fattura24 address book','fatt-24'),
                'help'  => __('Check this flag to save Customer data on fattura24 Server address book','fatt-24')
            ),
            
            // permettere all’utente di impostare il C.F. e/o la P.IVA come campi obbligatori
            ABK_FISCODE_REQ=>array(
                'type'  => 'bool',
                'label' => __('Fiscal Code requested','fatt-24'),
                'desc'  => __(' Fiscal Code verification requested', 'fatt-24'),
                'help'  => __('Check this flag to make the Fiscal Code verification requested', 'fatt-24')
            ),
            ABK_VATCODE_REQ=>array(
                'type'  => 'bool',
                'label' => __('Vat Code requested', 'fatt-24'),
                'desc'  => __(' Vat Code verification requested', 'fatt-24'),
                'help'  => __('Check this flag to make the Vat Code verification requested', 'fatt-24')
            )
        )
    );

    /*
    $sect_docloc = array(
        'section_header'    => '<hr/>'.__('Document location','fatt-24'),
        'section_callback'  => null,
        'section_id'        => OPT_DOCS_LOC_SECT_ID,
        'fields'            => array(
    
            // prepare for proper documents location
            OPT_DOCS_FOLDER=>array(
                'type'  => 'text_cmd',
                'label' => __('DOCS Folder', 'fatt-24'),
                'desc'  => __('set the path relative to wp-content/uploads', 'fatt-24'),
                'default' => 'fattura24',
                'cmd_id' => OPT_MOVE_DOCS,
                'cmd_text' => __('Copy','fatt-24'),
                'cmd_help' => __('Copy documents from old location to new.','fatt-24'),
            ),
        )
    );
    */
    
    /* Sezione Ordini */
    $sect_orders = array(
        'section_header'    => '<hr/>'.__('Orders','fatt-24'),
        'section_callback'  => null,
        'section_id'        => ORD_SECT_ID,
        'fields'            => array(
            // A. un flag di spunta: “Attiva creazione ordine cliente” per attivare o disattivare la creazione del documento d’ordine
            ORD_CREATE=>array(
                'type'  => 'bool',
                'default' => true,
                'label' => __('Create order','fatt-24'),
                'desc'  => __(' Enable creation of Customer Order document','fatt-24'),
                'help'  => __('Check this flag to enable Customer order document creation','fatt-24')
            ),
            /* not available right now
            // B. un flag di spunta “Scarica copia del PDF dell’ordine”
            //  a. se il flag al punto ‘A’ è su ‘SI’ questo nuovo flag deve essere attivo, altrimenti va tolta la spunta e reso ineditabile
            //  b. se questo flag è impostato su ‘SI’, il sistema dovrà scaricare il PDF dell’ordine dopo la sua creazione in Fattura24
            ORD_DOWNLOAD=>array(
                'type'  => 'bool',
                'label' => __('Download','fatt-24'),
                'desc'  => __('Enable download of Customer Order PDF document','fatt-24'),
                'help'  => array(
                    __('Check this flag to download Customer Order document creation.','fatt-24'),
                    __('PDF Customer Order document will be downloaded on your server only if this flag is enabled.','fatt-24')
                )
            ),
            */
            ORD_SEND=>array(
                'type'  => 'bool',
                'label' => __('Send email','fatt-24'),
                'desc'  => __(' Enable send of Customer Order PDF document from Fattura24 server','fatt-24'),
                'help'  => __('Check this flag to send via email the document to Customer','fatt-24')
            ),
            ORD_STOCK=>array(
                'type'  => 'bool',
                'label' => __('Warehouse handling','fatt-24'),
                'desc'  => __(' Enable warehouse handling with Order creation','fatt-24'),
                'help'  => __('Check this flag to enable warehouse handling with Order creation. Products that are in Fattura24 and have the same code of products in WooCommerce, will be reserved','fatt-24')
            ),
            ORD_TEMPLATE=>array(
                'type'  => 'select',
                'label' => __('Order model','fatt-24'),
                'desc'  => __(' Select the model to use for the creation of the Order PDF','fatt-24'),
                'help'  => __('Select the model to use for the creation of the Order PDF from the models you have in Fattura24. This model will be used if the Order doesn\'t contain a shipping address. If \'Predefinito\' is selected, the model choosed as default model in Fattura24 will be used. To view the list, before you have to save your Api Key in WooCommerce','fatt-24')
            ),
            ORD_TEMPLATE_DEST=>array(
                'type'  => 'select',
                'label' => __('Order model with destination','fatt-24'),
                'desc'  => __(' Select the model to use for the creation of the Order PDF when Order contains a shipping address','fatt-24'),
                'help'  => __('Select the model to use for the creation of the Order PDF from the models you have in Fattura24. This model will be used if the Order contains a shipping address. If \'Predefinito\' is selected, the model choosed as default model in Fattura24 will be used. To view the list, before you have to save your Api Key in WooCommerce','fatt-24')
            )
        )
    );

    /* Sezione Fattura */
    $sect_invoices = array(
        'section_header'    => '<hr/>'.__('Invoices','fatt-24'),
        'section_callback'  => null,
        'section_id'        => INV_SECT_ID,
        'fields'            => array(
            INV_CREATE=>array(
                'type'  => 'bool',
                'default' => true,
                'label' => __('Create invoice','fatt-24'),
                'desc'  => __(' Enable Invoice creation and download automatically when Order Status is Completed','fatt-24'),
                'help'  => __('The Invoice about a customer order will be automatically created on Fattura24 server','fatt-24')
            ),
            /*
            INV_DOWNLOAD=>array(
                'type'  => 'bool',
                'default' => true,
                'label' => __('Download PDF','fatt-24'),
                'desc'  => __(' Get invoice from Fattura24 server to WordPress server automatically when Order Status is Completed','fatt-24'),
                'help'  => __('Enable automatic Invoice Download from Fattura24 server to WordPress server','fatt-24')
            ),
            */
            INV_SEND=>array(
                'type'  => 'bool',
                'label' => __('Send email','fatt-24'),
                'desc'  => __(' Send Invoice to customer by email automatically when Order Status is Completed','fatt-24'),
                'help'  => __('Enable automatic sending of invoice from Fattura24 server to customer','fatt-24')
            ),
            INV_STOCK=>array(
                'type'  => 'bool',
                'label' => __('Warehouse handling','fatt-24'),
                'desc'  => __(' Enable warehouse handling with Invoice creation','fatt-24'),
                'help'  => __('Check this flag to enable warehouse handling with Invoice creation. Products that are in Fattura24 and have the same code of products in WooCommerce, will be unloaded','fatt-24')
            ),
            INV_WHEN_PAYED=>array(
                'type'  => 'bool',
                'label' => __('Status Payed','fatt-24'),
                'desc'  => __(' Create invoice directly with status Payed','fatt-24'),
                'help'  => __('Check this flag to create invoice directly with status Payed','fatt-24')
            ),
            INV_DISABLE_RECEIPTS=>array(
                'type'  => 'bool',
                'label' => __('Disable creation of receipts','fatt-24'),
                'desc'  => __(' Create an invoice instead of a receipt even when the VAT number of the customer is absent','fatt-24'),
                'help'  => __('Check this flag to enable the creation of an invoice instead of a receipt even when the VAT number of the customer is absent','fatt-24')
            ),
            INV_TEMPLATE=>array(
                'type'  => 'select',
                'label' => __('Invoice model','fatt-24'),
                'desc'  => __(' Select the model to use for the creation of the Invoice PDF','fatt-24'),
                'help'  => __('Select the model to use for the creation of the Invoice PDF from the models you have in Fattura24. This model will be used if the Order doesn\'t contain a shipping address. If \'Predefinito\' is selected, the model choosed as default model in Fattura24 will be used. To view the list, before you have to save your Api Key in WooCommerce','fatt-24')
            ),
            INV_TEMPLATE_DEST=>array(
                'type'  => 'select',
                'label' => __('Invoice model with destination','fatt-24'),
                'desc'  => __(' Select the model to use for the creation of the Invoice PDF when Order contains a shipping address','fatt-24'),
                'help'  => __('Select the model to use for the creation of the Invoice PDF from the models you have in Fattura24. This model will be used if the Order contains a shipping address. If \'Predefinito\' is selected, the model choosed as default model in Fattura24 will be used. To view the list, before you have to save your Api Key in WooCommerce','fatt-24')
            ),
            INV_PDC=>array(
                'type'  => 'select',
                'label' => __('Account planning','fatt-24'),
                'desc'  => __(' You can select the economic account that will be associated with the items of the documents','fatt-24'),
                'help'  => __('You can select the economic account that will be associated with the items of the invoice, from your economic accounts in Fattura24. To view this list, before you have to save your Api Key in WooCommerce','fatt-24')
            ),
            INV_SEZIONALE_RICEVUTA=>array(
                'type'  => 'select',
                'label' => __('Receipts numerator','fatt-24'),
                'desc'  => __(' You can select the numerator of the receipts','fatt-24'),
                'help'  => __('You can select the numerator of the receipts, from the list of your numerators in Fattura24 that you setted as \'active\' for receipts. To view this list, before you have to save your Api Key in WordPress','fatt-24')
            ),
            INV_SEZIONALE_FATTURA=>array(
                'type'  => 'select',
                'label' => __('Invoices numerator','fatt-24'),
                'desc'  => __(' You can select the numerator of the invoices','fatt-24'),
                'help'  => __('You can select the numerator of the invoices, from the list of your numerators in Fattura24 that you setted as \'active\' for invoices. To view this list, before you have to save your Api Key in WordPress','fatt-24')
            )
        )
    );

    setup_settings_page(SETTINGS_PAGE, SETTINGS_GROUP, array($sect_key, $sect_addrbook, /*$sect_docloc,*/ $sect_orders, $sect_invoices));
}

// display a specific settings page
function show_settings() {
    //page(__('Settings', 'fatt-24'));
    page();
    ?>
    <div class='wrap'>
    <form method='post' action='options.php'>
        <?php
        submit_button(__('Save Settings!', 'fatt-24'));
        do_settings_sections(SETTINGS_PAGE);
        settings_fields(SETTINGS_GROUP);
        submit_button(__('Save Settings!', 'fatt-24'));
        ?>
    </form>
    </div>
    <script>
        jQuery(function() {
            var $ = jQuery;
            <?php
            $api_key = get_option("fatt-24-API-key");
            $ord_template = get_option("fatt-24-ord-template");
            $ord_template_dest = get_option("fatt-24-ord-template-dest");
            $inv_template = get_option("fatt-24-inv-template");
            $inv_template_dest = get_option("fatt-24-inv-template-dest");
            $inv_pdc = get_option("fatt-24-inv-pdc");
            $inv_sezionale_ricevuta = get_option("fatt-24-inv-sezionale-ricevuta");
            $inv_sezionale_fattura = get_option("fatt-24-inv-sezionale-fattura");
            ?>

            var ord_template_select = document.getElementById("fatt-24-ord-template");
            var ord_template_dest_select = document.getElementById("fatt-24-ord-template-dest");
            <?php
            foreach(getTemplate(true) as $modello)
            { ?>
                var option = document.createElement("option");
                option.text = '<?=$modello?>';
                ord_template_select.add(option);
                ord_template_dest_select.add(option.cloneNode(true));
            <?php } ?>
            ord_template_select.value = '<?php
            if(empty($ord_template) || empty($api_key))
                echo 'Predefinito';
            else
                echo $ord_template;?>';
            ord_template_dest_select.value = '<?php
            if(empty($ord_template_dest) || empty($api_key))
                echo 'Predefinito';
            else
                echo $ord_template_dest;?>';
            
            var inv_template_select = document.getElementById("fatt-24-inv-template");
            var inv_template_dest_select = document.getElementById("fatt-24-inv-template-dest");
            <?php
            foreach(getTemplate(false) as $modello)
            { ?>
                var option = document.createElement("option");
                option.text = '<?=$modello?>';
                inv_template_select.add(option);
                inv_template_dest_select.add(option.cloneNode(true));
            <?php } ?>
            inv_template_select.value = '<?php
            if(empty($inv_template) || empty($api_key))
                echo 'Predefinito';
            else
                echo $inv_template;?>';
            inv_template_dest_select.value = '<?php
            if(empty($inv_template_dest) || empty($api_key))
                echo 'Predefinito';
            else
                echo $inv_template_dest;?>';
            
            var pdc_select = document.getElementById("fatt-24-inv-pdc");
            <?php
            foreach (getPdc() as $pdc)
            { ?>
                var option = document.createElement("option");
                option.text = '<?=$pdc?>';
                pdc_select.add(option);
            <?php } ?>
            pdc_select.value = '<?php
            if(empty($inv_pdc) || empty($api_key))
                echo 'Nessun Pdc';
            else
                echo $inv_pdc;?>';
            
            var inv_sezionale_ricevuta_select = document.getElementById("fatt-24-inv-sezionale-ricevuta");
            <?php
            foreach (getSezionale(3) as $sezionale)
            { ?>
                var option = document.createElement("option");
                option.text = '<?=$sezionale?>';
                inv_sezionale_ricevuta_select.add(option);
            <?php } ?>
            inv_sezionale_ricevuta_select.value = '<?php
            if(empty($inv_sezionale_ricevuta) || empty($api_key))
                echo 'Predefinito';
            else
                echo $inv_sezionale_ricevuta;?>';
            
            var inv_sezionale_fattura_select = document.getElementById("fatt-24-inv-sezionale-fattura");
            <?php
            foreach (getSezionale(1) as $sezionale)
            { ?>
                var option = document.createElement("option");
                option.text = '<?=$sezionale?>';
                inv_sezionale_fattura_select.add(option);
            <?php } ?>
            inv_sezionale_fattura_select.value = '<?php
            if(empty($inv_sezionale_fattura) || empty($api_key))
                echo 'Predefinito';
            else
                echo $inv_sezionale_fattura;?>';



            // request to shrink the space between 'API KEY' label and input field
            $('.form-table th').css('width','160px');

            var OPT_API_VERIFICATION = '#<?php echo OPT_API_VERIFICATION?>';
            var OPT_API_KEY = '#<?php echo OPT_API_KEY?>';
            $(OPT_API_VERIFICATION).click(function() {
                $.post(ajaxurl, {
                    action: 'api_verification',
                    security: '<?php echo wp_create_nonce(FATT_24_API_VERIF_NONCE)?>',
                    api_key: $(OPT_API_KEY).val()
                }, function(ans) {
                    alert("<?php _e('API outcome    :','fatt-24')?>" + ans.description)
                })
            })

            function cb_controlled(main, controlled) {
                var checked = $(main).is(':checked')
                var group = controlled.join(',')
                if (checked) {
                    $(group).prop('disabled', false)
                }
                else {
                    $(group).prop('disabled', true)
                    $(group).prop('checked', false)
                }
            }

            var ORD_CREATE            = '#<?php echo ORD_CREATE?>',
                ORD_SEND              = '#<?php echo ORD_SEND?>',
                ORD_STOCK             = '#<?php echo ORD_STOCK?>',
                ORD_TEMPLATE          = '#<?php echo ORD_TEMPLATE?>',
                ORD_TEMPLATE_DEST     = '#<?php echo ORD_TEMPLATE_DEST?>';

            var INV_CREATE            = '#<?php echo INV_CREATE?>',
                INV_SEND              = '#<?php echo INV_SEND?>',
                INV_STOCK             = '#<?php echo INV_STOCK?>',
                INV_WHEN_PAYED        = '#<?php echo INV_WHEN_PAYED?>',
                INV_DISABLE_RECEIPTS  = '#<?php echo INV_DISABLE_RECEIPTS?>',
                INV_TEMPLATE          = '#<?php echo INV_TEMPLATE?>',
                INV_TEMPLATE_DEST     = '#<?php echo INV_TEMPLATE_DEST?>',
                INV_PDC               = '#<?php echo INV_PDC?>',
                INV_SEZIONALE_RICEVUTA= '#<?php echo INV_SEZIONALE_RICEVUTA?>',
                INV_SEZIONALE_FATTURA = '#<?php echo INV_SEZIONALE_FATTURA?>';
                
            $(ORD_CREATE).click(function() {
                cb_controlled(ORD_CREATE, [ORD_SEND, ORD_STOCK, ORD_TEMPLATE, ORD_TEMPLATE_DEST])
            })
            $(INV_CREATE).click(function() {
                cb_controlled(INV_CREATE, [INV_SEND, INV_STOCK, INV_WHEN_PAYED, INV_DISABLE_RECEIPTS, 
                    INV_TEMPLATE, INV_TEMPLATE_DEST, INV_PDC, INV_SEZIONALE_RICEVUTA, INV_SEZIONALE_FATTURA])
            })

            cb_controlled(ORD_CREATE, [ORD_SEND, ORD_STOCK, ORD_TEMPLATE, ORD_TEMPLATE_DEST])
            cb_controlled(INV_CREATE, [INV_SEND, INV_STOCK, INV_WHEN_PAYED, INV_DISABLE_RECEIPTS, 
                INV_TEMPLATE, INV_TEMPLATE_DEST, INV_PDC, INV_SEZIONALE_RICEVUTA, INV_SEZIONALE_FATTURA])
            
            var ABK_SAVE_CUST_DATA = '#<?php echo ABK_SAVE_CUST_DATA?>';
            function enable_when_both_off() {
                var c1 = $(INV_CREATE).is(':checked')
                var c2 = $(ORD_CREATE).is(':checked')
                $(ABK_SAVE_CUST_DATA).prop('checked', c1 || c2)
                $(ABK_SAVE_CUST_DATA).prop('disabled', c1 || c2)
            }
            $(INV_CREATE+','+ORD_CREATE).click(function() {
                enable_when_both_off();
            })
            enable_when_both_off()
        })
    </script>
    <?php
}