<?php

/**
 * Description: helpers to build Wordpress settings page
 * Author: Capelli Carlo
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'uty.php';
require_once 'constants.php';

function setup_settings_page($page_id, $group_id, $sections) {

    foreach($sections as $sect) {
        add_settings_section(
            $sect['section_id'],
            $sect['section_header'],
            $sect['section_callback'],
            $page_id
        );
        foreach($sect['fields'] as $id => $def) {
            // Add the field with the names and function to use for our new
            // settings, put it in our new section

            $args = array('id' => $id);

            // make (optionally) multiline help icon
            if (isset($def['help']))
                $args['help'] = array_string($def['help'], "\n");

            foreach(array('desc', 'text', 'size', 'default', 'cmd_id', 'cmd_text', 'cmd_help', 'func', 'class', 'options') as $arg)
                if (isset($def[$arg]))
                    $args[$arg] = $def[$arg];

            add_settings_field(
                $id,
                $def['label'],
                __NAMESPACE__ .'\setting_field_'.$def['type'].'_callback',
                $page_id,
                $sect['section_id'],
                $args
            );

            // Register our setting so that $_POST handling is done for us and
            // our callback function just has to echo the <input>
            register_setting($group_id, $id);
        }
    }
}

/* display an Help Icon drawn from WordPress prebuilt icon set */
function helpico($text) {
    //return apply_filters(LAYOUT_ADMIN_HELPER, $text);
    return span(array('title'=>$text, 'class'=>'dashicons dashicons-editor-help'/*, 'style'=>'margin-left:10px'*/), array());
}
    
function setting_field_output($widget, $help, $desc) {
    echo apply_filters(LAYOUT_OPTION, compact('widget', 'help', 'desc'));
    /*
    echo $widget.'<br>';
    if ($help) echo helpico($help);
    if ($desc) echo $desc;
    */
}

function widget_bool($id, $class) {
    return input(array(
        'name'  => $id,
        'id'    => $id,
        'type'  => 'checkbox',
        'value' => '1',
        'class' => $class,
        checked(1, get_option($id), false)
    ));
}
function setting_field_bool_callback(array $args) {
    extract(shortcode_atts(array(
        'id'    => null,
        'desc'  => null,
        'help'  => null,
        'default' => null
    ),  $args));

    if ($default !== null) {
        global $wpdb;
        $c = $wpdb->get_results("select * from $wpdb->options where option_name='$id'");
        if ($c === array())
            update_option($id, $default);
    }

    //$widget = '<input name="'.$id.'" id="'.$id.'" type="checkbox" value="1" class="code" '.checked(1, get_option($id), false).' />';
    $widget = widget_bool($id, 'code');
    setting_field_output($widget, $help, $desc);
}

function widget_radio($id, $class, $options) {
    $widget = '';
    foreach($options as $k => $v) {
        $ref = sprintf('%s-%s', $id, $k);
        $widget .=
            label(array('for'=>$ref), $v) .
            input(array('name'=>$id, 'id'=>$ref, 'type'=>'radio', 'value'=>$k, 'class'=>$class, 0=>checked($k, get_option($id), false)));
    }
    return $widget;
}
function setting_field_radio_callback(array $args) {
    extract(shortcode_atts(array(
        'id'        => null,
        'desc'      => null,
        'help'      => null,
        'options'   => array(),
        'class'     => null
    ),  $args));

    $widget = widget_radio($id, $class, $options);
    setting_field_output($widget, $help, $desc);
}

function widget_select($id, $class, $current, $options) {
    return select(array('name'=>$id, 'id'=>$id, 'class'=>$class),
        array_map_kv(function($k, $v) use($current) {
            return option(array('value' => $k, selected($k, $current, false)), $v);
        }, $options));
}
function setting_field_select_callback(array $args) {
    extract(shortcode_atts(array(
        'id'        => null,
        'class'     => null,
        'options'   => array(),
        'desc'      => null,
        'help'      => null,
    ),  $args));

    $current = get_option($id);
    $widget = widget_select($id, $class, $current, $options);
    setting_field_output($widget, $help, $desc);
}

function setting_field_text_callback(array $args) {
    extract(shortcode_atts(array(
        'id'    => null,
        'desc'  => null,
        'help'  => null,
        'class' => null,
        'size'  => 32,
        'default' => null
    ),  $args));
    
    $value = get_option($id);
    if (empty($value))
        $value = $default;
        
    setting_field_output(
        input(array(
            'type'  => 'text',
            'name'  => $id,
            'id'    => $id,
            'value' => $value,
            'class' => $class,
            'size'  => $size,
        )),
        $help, $desc);
}
function setting_field_button_callback(array $args) {
    extract(shortcode_atts(array(
        'id'    => null,
        'desc'  => null,
        'help'  => null,
        'text'  => null
    ),  $args));
    
    setting_field_output(
        input(array(
            'type'  => 'button',
            'name'  => $id,
            'id'    => $id,
            'class' => 'code',
            'value' => $text
        )), $help, $desc);
}
function setting_field_label_callback(array $args) {
    extract(shortcode_atts(array(
        'id'    => null,
        'desc'  => null,
        'help'  => null,
        'text'  => null,
    ),  $args));
    
    setting_field_output(label(id($id), $text), $help, $desc);
}
function setting_field_text_cmd_callback(array $args) {
    extract(shortcode_atts(array(
        'id'        => null,
        'desc'      => null,
        'help'      => null,
        'size'      => 32,
        'class'     => 'code',
        'default'   => null,
        
        'cmd_id'    => null,
        'cmd_text'  => null,
        'cmd_help'  => null
    ),  $args));
    
    $widget = span(array(
        input(array(
            'type'  => 'text',
            'name'  => $id,
            'id'    => $id,
            'value' => get_option($id, $default),
            'class' => $class,
            'size'  => $size
        )),
        input(array(
            'type'  => 'button',
            'id'    => $cmd_id,
            'class' => 'code',
            'value' => $cmd_text,
            'title' => $cmd_help
        ))
    ));
    setting_field_output($widget, $help, $desc);
}

function setting_field_table_callback(array $args) {
    extract(shortcode_atts(array(
        'id'    => null,
        'desc'  => null,
        'help'  => null,
        'func'  => null,
    ),  $args));
    assert('$id != null');
    setting_field_output(label(id($id), call_user_func($func)), $help, $desc);
}

/* a tabletree is a table with fold/unfold capabilities
 *  javascript handles operations, based on consecutive rows' values
function setting_field_tabletree_callback(array $args) {
    extract(shortcode_atts(array(
        'id'    => null, //__NAMESPACE__ .'_label',
        'desc'  => null,
        'help'  => null,
        'func'  => null,
    ),  $args));
    assert($id);
    setting_field_output(label(id($id), call_user_func($func)), $help, $desc);
}
*/
