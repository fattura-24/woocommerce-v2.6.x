function f24_pdfcmd(id,cmd,doc_type,nonce) {
    jQuery('.wrap').css('cursor','wait');
    jQuery.post(ajaxurl, {
        action: 'invoice_admin_command',
        security: nonce,
        args: { id:id, cmd:cmd, type:doc_type }
    }).done(function(r) {
        if (r[0] == 1) {
            if (cmd == 'update')
                alert('Download da Fattura24 completato')
            jQuery('#cmds-'+id).html(r[1]);
        } else {
            alert(r[1]);
        }
    }).fail(function(err){
        console.log(arguments);
    })
    .always(function(){
        jQuery('.wrap').css('cursor','auto');
    })
}