<?php

/**
 * Fattura24.com
 * Description: define default behaviour, to be extended by specialized needs
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'uty.php';
require_once 'constants.php';

if (!is_admin())
    require_once ABSPATH.'wp-admin/includes/file.php';
    
add_filter(DOC_PDF_FILENAME, function($args) {
    $doc_type = $args['doc_type'];
    $order_id = $args['order_id'];
    $timestamp = now('YmdHis');
    return sprintf('doc-%s-%s-%s.pdf', $timestamp, $doc_type, $order_id);
});

// access the order metadata
add_filter(ORDER_GET_VAT, function($order) {
    return get_post_meta($order->id, '_billing_vatcode', true);
});
function order_p_iva($order) {
    return apply_filters(ORDER_GET_VAT, $order);
}

add_filter(ORDER_GET_CF, function($order) {
    return get_post_meta($order->id, '_billing_fiscalcode', true);
});
function order_c_fis($order) {
    return apply_filters(ORDER_GET_CF, $order);
}

function customer_use_vat() {
    return apply_filters(CUSTOMER_USE_VAT, null);
}
add_filter(CUSTOMER_USE_VAT, function() { return true; });

function customer_use_cf() {
    return apply_filters(CUSTOMER_USE_CF, null);
}
add_filter(CUSTOMER_USE_CF, function() { return true; });

function get_url_from_file($file) {
    $hp = get_home_path();
    $last_hp_ = explode('/',substr($hp,0,-1));
    $last_hp = end($last_hp_);
    $last_file_ = explode($last_hp,$file);
    $last_file = end($last_file_);
    return home_url().$last_file;
}

// document related locations
function PDF_filename($doc_type, $order_id) {
    /*
    $timestamp = now('YmdHis');
    return sprintf('doc-%s-%s-%s.pdf', $timestamp, $doc_type, $order_id);
    */
    return apply_filters(DOC_PDF_FILENAME, compact('doc_type', 'order_id'));
}

function set_file_permissions($new_file) {
    // Set correct file permissions
    $stat = @stat(dirname($new_file));
    $perms = $stat['mode'] & 0007777;
    $perms = $perms & 0000666;
    @chmod($new_file, $perms);
}

add_filter(DOC_STORE_FILE, function($status, $orderId, $PDF) {
    
    $docType = peek($status, 'docType', DT_FATTURA);
    $oldPdfPath = peek($status, 'pdfPath');

    $file = PDF_filename($docType, $orderId);
    $folder = trailingslashit(DOCS_FOLDER);
    
    $wpdir = wp_upload_dir();
    $basedir = $wpdir['basedir'];

    $dir = $basedir.'/'.$folder;
    //if (!file_exists($dir))
        wp_mkdir_p($dir);
    
    $new_file = $dir.$file;

    $ifp = @fopen($new_file, 'wb');
    if (!$ifp)
        order_status_set_error($status, sprintf(__('Could not write file %s', 'fatt-24'), $new_file));
    else {
        @fwrite($ifp, $PDF);
        fclose($ifp);
        set_file_permissions($new_file);
        order_status_set_file_data($status, $new_file, $docType);

        if ($oldPdfPath && is_file($oldPdfPath))
            unlink($oldPdfPath);
    }

    return $status;
}, 10, 3);

add_filter(PRODUCT_ITEM_DESC, function($item) {
    //trace('item', $item);
    return $item['name'];
});

add_filter(DOC_OBJECT, function($order) {
    return null;
});

function doc_footnotes($order) {
    $FootNotes = sprintf(__('order num. %d', 'fatt-24'), $order->get_order_number());
    if ($order->customer_message)
        $FootNotes .= ' - ' . $order->customer_message;
    return $FootNotes;
}
add_filter(DOC_FOOTNOTES, function($order) {
    return doc_footnotes($order);
});

add_filter(ON_STATUS_CHANGE, function($order) {
    return $order->post_status == 'wc-completed'
     || $order->post_status == 'wc-processing' && isset($order->payment_method) && $order->payment_method == 'paypal';
});

add_filter(DOC_PRODUCT_CODE, function($item) {
    $pid = $item['product_id'];
    $product = new \WC_Product($pid);
    return $product->get_sku();
});

add_filter(DOC_ADDRESS, function($order) {
    return make_strings(    array($order->billing_address_1, $order->billing_address_2),
                            array($order->shipping_address_1, $order->shipping_address_2));
});

add_filter(DOC_SHIPPING_CALC, function($args) {
    $item = $args['item'];
    
    $cost = $item['cost'];
    $taxt = taxes_total($item['taxes']);
    $vat = vat_from_price_and_tax($cost, $taxt);
    
    $args['cost'] = $cost;
    $args['vat'] = $vat;
    $args['tax'] = $taxt;
    
    return $args;
});

add_filter(LAYOUT_OPTION, function($args) {
    extract($args);
    $rc = $widget;
    if ($help) $rc .= helpico($help);
    if ($desc) $rc .= $desc;
    return $rc;
});

/*
 * run once: move documents to fixed path folder, *outside* fattura24 folder
 * update postmeta records with new path
 */
function move_docs($folder) {
    //trace('move_docs', $folder);
    $folder = trailingslashit($folder);

    $wpdir = wp_upload_dir();
    $basedir = $wpdir['basedir'];

    $dir = $basedir.'/'.$folder;
    wp_mkdir_p($dir);
    
    $recs_postmeta = 0;
    $ok_meta_value = 0;
    $ko_meta_value = 0;
    $has_pdfPath = 0;
    $no_pdfPath = 0;
    $moved = 0;
    $cant_move = 0;
    
    global $wpdb;
    foreach($wpdb->get_results($wpdb->prepare("SELECT * FROM $wpdb->postmeta WHERE meta_key=%s", ORDER_INVOICE_STATUS)) as $r) {
        ++$recs_postmeta;
        if ($i = maybe_unserialize($r->meta_value)) {
            ++$ok_meta_value;
            if (isset($i['pdfPath']) && is_file($i['pdfPath'])) {
                ++$has_pdfPath;
                $old = $i['pdfPath'];
                $file = basename($old);
                $new = $dir.$file;
                if ($new != $old && rename($old, $new)) {
                    set_file_permissions($new);
                    ++$moved;
                    $i['pdfPath'] = $new;
                    store_order_status($r->post_id, $i);
                }
                else
                    ++$cant_move;
            }
            else
                ++$no_pdfPath;
        }
        else
            ++$ko_meta_value;
    }

    return compact(
        'recs_postmeta',
        'ok_meta_value',
        'ko_meta_value',
        'has_pdfPath',
        'no_pdfPath',
        'moved',
        'cant_move'
    );
}

add_filter(COUPON_DESCRIPTION, function($coupon) {
    if ($post = get_post($coupon->id))
        if (!empty($post->post_excerpt))
            return $post->post_excerpt;
    return $coupon->code;
});
