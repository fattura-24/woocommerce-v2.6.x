<?php

/**
 * Fattura24.com
 * Description: handle WooCommerce hooks
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

require_once 'settings.php';
require_once 'api_call.php';
require_once 'uty.php';
require_once 'order_status.php';

// reference: https://docs.woothemes.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/

function order_action($order_id) {
    $order = new \WC_Order($order_id);
    $transient_id = 'order_action-'.$order_id;
    if (get_transient($transient_id) === false) {
        set_transient($transient_id, true, 60);
        process_order($order_id);
        process_customer($order_id);
        delete_transient($transient_id);
    }
    else
        trace('transient blocked', $transient_id);
    
}

function validate_order($order_id)
{
    $order = new \WC_Order($order_id);
    $dati_utente = get_user_meta($order->get_user_id());
    if($dati_utente)
        return true;
    return false;
}

/*
- woocommerce_thankyou : non funziona sempre per ordini pagati con paypal, perché in questi ordini non viene necessariamente visualizzata la pagina di ringraziamento di WooCommerce;
- woocommerce_checkout_update_order_meta : con questo hook non vengono inviati a Fattura24 i campi codice_fiscale e partita_iva che vengono evidentemente assegnati all'ordine dopo;
- woocommerce_checkout_order_processed : funziona bene per ordini creati da customer;
- save_post_shop_order : funziona anche se si crea l'ordine da admin. Viene chiamata anche da customer, ma non per acquisti tramite paypal. Viene chiamata anche appena viene creato il post 'ordine' prima che venga riempito. Quando si clicca su 'Crea ordine' in WooCommerce 3.x viene chiamata con un ordine senza dati. Quando si aggiorna l'ordine va bene. In WooCommerce 2.6.x invece viene chiamata due volte, la seconda con l'ordine completo, in modo da risolvere il problema;
- wp_insert_post : come save_post ma con la differenza che quando si clicca su 'Crea ordine' in WooCommerce 3.x viene chiamata con un ordine completo.
Alternative:
" get_post_status($order_id) !== 'auto-draft' " al posto di " validate_order($order_id) ". Ma da customer è sempre verificata e quindi si attiva una volta con un ordine incompleto
" current_user_can('manage_options') " al posto di " !did_action('woocommerce_checkout_order_processed') ". Ma se l'admin crea un ordine lato customer si attiva sempre
*/

// creazione ordine da customer
add_action('woocommerce_checkout_order_processed', function($order_id) {
    order_action($order_id);
});

// creazione ordine da admin
add_action('wp_insert_post', function($order_id) {
    if(!did_action('woocommerce_checkout_order_processed') && get_post_type($order_id) == 'shop_order' && validate_order($order_id))
        order_action($order_id);
});

// creazione fattura
add_action('woocommerce_order_status_completed', function($order_id) {
    process_fattura($order_id);
    if(!is_PDF_available($order_id, DT_FATTURA))
        download_PDF($order_id);
});

/*
 * Add fields 'Fiscal Code' and 'VAT Code' to the checkout page
 * https://docs.woocommerce.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 */
 add_action('woocommerce_checkout_fields', function($fields) {
    $fields['billing']['billing_fiscalcode'] = array(
        'type'       => 'text',
        'label'      => __('Fiscal Code', 'fatt-24'),
        'required'   => get_flag(ABK_FISCODE_REQ),
        'class'      => array('form-row-wide'),
        'priority'   => 31,
        'clear'      => true
     );
     $fields['billing']['billing_vatcode'] = array(
        'type'       => 'text',
        'label'      => __('VAT Code', 'fatt-24'),
        'required'   => get_flag(ABK_VATCODE_REQ),
        'class'      => array('form-row-wide'),
        'priority'   => 31,
        'clear'      => true
     );
     return $fields;
});

/*
 * must handle the metadata transfer, since woocommerce_form_field doesn't care
 */
add_action('woocommerce_checkout_update_order_meta', function($order_id) {
    if (!empty($_POST['billing_fiscalcode'])) {
        update_post_meta($order_id, '_billing_fiscalcode', sanitize_text_field($_POST['billing_fiscalcode']));
    }
    if (!empty($_POST['billing_vatcode'])) {
        update_post_meta($order_id, '_billing_vatcode', sanitize_text_field($_POST['billing_vatcode']));
    }
});

/*
 * Add new register fields for WooCommerce registration:
 * Fiscal Code and VAT Code to user registration form, documentation here:
 * https://support.woothemes.com/hc/en-us/articles/203182373-How-to-add-custom-fields-in-user-registration-on-the-My-Account-page
 * URL changed: https://claudiosmweb.com/2016/03/24/how-to-add-custom-fields-in-user-registration-on-the-my-account-page/
 */
add_action('woocommerce_register_form_start', function() {

    ?>

    <?php if (customer_use_cf()) {    ?>
        <p class="form-row form-row-first">
        <label for="reg_billing_fiscal_code"><?php _e('Fiscal Code', 'fatt-24'); ?></label>
        <input type="text" class="input-text"
            name="billing_fiscalcode" id="reg_billing_fiscal_code"
            value="<?php if (!empty($_POST['billing_fiscalcode'])) esc_attr_e($_POST['billing_fiscalcode']); ?>" />
        </p>
    <?php }    ?>

    <?php if (customer_use_vat()) {    ?>
        <p class="form-row form-row-last">
        <label for="reg_billing_VAT_code"><?php _e('VAT Code', 'fatt-24'); ?>  <?php if (get_flag(ABK_VATCODE_REQ)) echo '<span class="required">*</span>'?> </label>
        <input type="text" class="input-text"
            name="billing_vatcode" id="reg_billing_VAT_code"
            value="<?php if (!empty($_POST['billing_vatcode'])) esc_attr_e($_POST['billing_vatcode']); ?>" />
        </p>
    <?php }    ?>

    <?php
});

/**
 * Save the extra register fields.
 * @param  int $customer_id Current customer ID.
 * @return void
 */
add_action('woocommerce_created_customer', function($customer_id) {
    if (isset($_POST['billing_fiscalcode'])) {
        update_user_meta($customer_id, 'billing_fiscalcode', sanitize_text_field($_POST['billing_fiscalcode']));
    }
    if (isset($_POST['billing_vatcode'])) {
        update_user_meta($customer_id, 'billing_vatcode', sanitize_text_field($_POST['billing_vatcode']));
    }
});

add_filter('woocommerce_customer_meta_fields', function($fields) {
    if (customer_use_cf() || customer_use_vat()) {
        if (!isset($fields['fatt_24']))
            $fields['fatt_24'] = array(
                'title' => __('Invoicing required fields', 'fatt-24'),
                'fields' => array()
            );
        if (customer_use_cf())
            $fields['fatt_24']['fields']['billing_fiscalcode'] = array(
                'label' => __('Fiscal Code', 'fatt-24'),
                'description' => __('a valid fiscal code', 'fatt-24'),
            );
        if (customer_use_vat())
            $fields['fatt_24']['fields']['billing_vatcode'] = array(
                'label' => __('VAT Code', 'fatt-24'),
                'description' => __('a valid VAT code', 'fatt-24'),
            );
    }
    return $fields;
});

function user_fiscalcode($user_id) {
    if(empty($user_id))
        return "";
    return get_the_author_meta('billing_fiscalcode', $user_id);
}
function user_vatcode($user_id) {
    if(empty($user_id))
        return "";
    return get_the_author_meta('billing_vatcode', $user_id);
}

add_action('woocommerce_edit_account_form', function() {
    if (customer_use_cf() || customer_use_vat()) {
        $user_id = get_current_user_id();
        $user = get_userdata($user_id);
        ?>
        <fieldset>
            <legend><?php _e('Fiscal Informations', 'fatt-24') ?></legend>

            <?php if (customer_use_cf()) { ?>
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    <label for="billing_fiscalcode"><?php _e('Fiscal Code', 'fatt-24'); ?></label>
                    <input type="text" class="woocommerce-Input input-text"
                        name="billing_fiscalcode" id="billing_fiscalcode"
                        value="<?php echo esc_attr(user_fiscalcode($user_id)) ?>" />
                    <span class="description"><?php _e('please enter a valid Fiscal Code', 'fatt-24'); ?></span>
                </p>
            <?php } ?>
            <?php if (customer_use_vat()) { ?>
                <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                    <label for="billing_vatcode"><?php _e('VAT Code', 'fatt-24')?></label>
                    <input type="text" class="woocommerce-Input input-text"
                        name="billing_vatcode" id="billing_vatcode"
                        value="<?php echo esc_attr(user_vatcode($user_id)) ?>" />
                    <span class="description"><?php _e('please enter a valid VAT Code', 'fatt-24'); ?></span>
                </p>
            <?php } ?>
        </fieldset>
        <div class="clear"></div>
        <?php
    }
});

add_action('woocommerce_save_account_details', function($user_id) {
    if (customer_use_cf())
        update_user_meta($user_id, 'billing_fiscalcode', htmlentities($_POST['billing_fiscalcode']));
    if (customer_use_vat())
        update_user_meta($user_id, 'billing_vatcode', htmlentities($_POST['billing_vatcode']));
});

add_action('woocommerce_admin_order_data_after_billing_address', function($order) {
    //$user_id = $order->user_id;
    if (customer_use_cf())
        echo p(strong(__('Fiscal Code', 'fatt-24')).':' . order_c_fis($order));
    //echo p(strong(__('Fiscal Code', 'fatt-24')).':' . user_fiscalcode($user_id));
    if (customer_use_vat())
        echo p(strong(__('VAT Code', 'fatt-24')).':' . order_p_iva($order));
        //echo p(strong(__('VAT Code', 'fatt-24')).':' . user_vatcode($user_id));
}, 10, 1);

add_action('woocommerce_admin_order_data_after_order_details', function($order) {
    echo div(array(h2(__('Fattura 24', 'fatt-24')), p(actions_of_order($order->id, DT_FATTURA))));
}, 10, 1);

// http://stackoverflow.com/questions/34785747/how-to-edit-woocommerce-admin-order-page
add_filter('manage_edit-shop_order_columns', function($columns) {
    $new_columns = is_array($columns) ? $columns : array();
    $new_columns['fatt24-invoice'] = __('Fattura24', 'fatt-24');
    return $new_columns;
});
add_action('manage_shop_order_posts_custom_column', function($column) {
    if ($column == 'fatt24-invoice') {
        global $post;
        $order = new \WC_Order($post->ID);
        echo actions_of_order($order->id, DT_FATTURA);
    }
}, 2);

// let customer view the PDF invoice, if available, in a new browser tab
// also consider to implement from http://stackoverflow.com/q/11733774/874024
add_filter('woocommerce_my_account_my_orders_actions', function($actions, $order) {
    if ($s = order_status($order->id))
        if ($s['status'] == INVSTA_PDF_AVAIL_LOCAL)
            $actions['pdfView'] = array(
                'url' => order_PDF_url($order->id),
                'name' => __('PDF Doc', 'fatt-24'),
            );
    return $actions;
}, 10, 2);
