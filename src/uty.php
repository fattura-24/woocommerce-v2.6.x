<?php

/**
 * Fattura24.com
 * Description: generic debug, array, HTML, path helpers.
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH')) exit;

/*
    debugging helpers
*/
function now($fmt = 'Y-m-d H:i:s', $tz = 'Europe/Rome') {
    $timestamp = time();
    $dt = new \DateTime("now", new \DateTimeZone($tz)); //first argument "must" be a string
    $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
    return $dt->format($fmt);
}

function trace() {
    if ($f = @fopen(plugin_dir_path(__FILE__).'trace.log', 'a')) {
        fprintf($f, "%s: %s\n", now(), var_export(func_get_args(), true));
        fclose($f);
    }
}
function ftrace() {
    if (true) {
        $args = func_get_args();
        $file = $args[0];
        $path_parts = pathinfo($file);
        $exclude = array();
        if (array_search($path_parts['filename'], $exclude) === false)
            if ($f = @fopen(plugin_dir_path($file).'trace.log', 'a')) {
                fprintf($f, "%s: %s\n", now(), var_export(array_slice($args, 1), true));
                fclose($f);
            }
    }
}

function fatal($why) {
    trace('fatal', $why, debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS));
    throw new \Exception('fatal error:'.$why);
}

/*
    arrays helpers
*/

/*
 * name: array_map_kv
 * @param
 * @return
 */
function array_map_kv($f, array $a) {
    return array_map(function($k) use ($f, $a) {
        return $f($k, $a[$k]);
    }, array_keys($a));
}

/*
 * name: array_map_values
 * @param
 * @return
 */
function array_map_values($f, array $a) {
    return array_values(array_map($f, $a));
}
function array_map_kv_values($f, array $a) {
    return array_values(array_map_kv($f, $a));
}

/*
 * name: array_string
 * @param
 * @return
 */
function array_string($m, $sep = '') {
    return is_array($m) ? implode($sep, $m) : $m;
}

/*
 * name: aggregate_array
 * @param
 * @return
 *  an array of arrays, indexed by return of $key_extract($value)
 */
function aggregate_array(array $array, $key_extract) {
    $aggregate = array();
    foreach($array as $value) {
        $k = $key_extract($value);
        if (!isset($aggregate[$k]))
            $aggregate[$k] = array();
        $aggregate[$k][] = $value;
    }
    return $aggregate;
}

function aggregate_names_store(array &$aggregate, $index, array $parts) {
    $N = array_shift($parts);
    if (!count($parts))
        $aggregate[$N][] = $index;
    else {
        if (!isset($aggregate[$N]))
            $aggregate[$N] = array();
        aggregate_names_store($aggregate[$N], $index, $parts);
    }
}
/*
function aggregate_names($names, $split_pattern = "_") {
    $aggregate = array();
    foreach(array_map(function($name) use ($split_pattern) { return split($split_pattern, $name); }, $names) as $index => $parts)
        aggregate_names_store($aggregate, $index, $parts);
    return $aggregate;
}
*/
if (!function_exists('array_column')) { // PHP 5.5
    function array_column(array $a, $c, $k = null) {
        $ret = array();
        if ($k)
            foreach($a as $r)
                $ret[$r[$k]] = $r[$c];
        else
            foreach($a as $r)
                $ret[] = $r[$c];
        return $ret;
    }
}

/*
 * name: numeric_keys_array
 * @param $array
 *  values to reindex
 * @param $start
 *  indexing start
 * @param $stride
 *  indexing increment
 * @return
 *  array having numeric keys from $start to $start+count($array)
 */
function numeric_keys_array(array $array, $start = 0, $stride = 1) {
    $r = array();
    foreach ($array as $v) {
        $r[$start] = $v;
        $start += $stride;
    }
    return $r;
}

/*
 *
 * name: position_in_array
 * @param
 * @return
 *
 */
function position_in_array($needle, array $haystack) {
    $k = array_search($needle, $haystack);
    return $k === false ? false : array_search($k, array_keys($haystack));
}

/*
 *
 * name: wp_league::position_in_array_keys
 * @param
 * @return
 *
 */
function position_in_array_keys($needle, array $haystack) {
    return array_search($needle, array_keys($haystack));
}

/*
    HTML helpers
*/
function attr($k, $v) { return array( $k => $v ); }
function align($pos) { return attr('text-align', $pos); }
function klass($klass) { return attr('class', $klass); }
function title($title) { return attr('title', $title); }
function id($id) { return attr('id', $id); }

function style(array $properties) {
    return attr('style', implode(';', array_map_kv(function($k, $v) { return is_int($k) ? $v : "$k:$v"; }, $properties)));
}
function attributes(array $attrs) {
    return array_map_kv(function($k, $v) { return is_int($k) ? $v : "$k=\"$v\""; }, $attrs);
}

/*
 * name: tag
 * @param
 *  tag open/close name
 * @param
 *  $A1 ($A2 ? array or string of attributes : array or string of HTML)
 *  $A2 optional array or string of HTML
 * @return
 *  well formed HTML tag with optional attributes
 */
function tag($tag, $A1, $A2) {

    if (isset($A2)) { // $A1: attributes
        if (is_array($A1))
            $A1 = array_string(attributes($A1), ' ');
        return sprintf('<%s %s>%s</%s>', $tag, $A1, array_string($A2, "\n"), $tag);
    }

    return sprintf('<%s>%s</%s>', $tag, array_string($A1, "\n"), $tag);
}

function a      ($A1, $A2 = null) { return tag('a', $A1, $A2); }
function b      ($A1, $A2 = null) { return tag('b', $A1, $A2); }
function i      ($A1, $A2 = null) { return tag('i', $A1, $A2); }
function p      ($A1, $A2 = null) { return tag('p', $A1, $A2); }
function th     ($A1, $A2 = null) { return tag('th', $A1, $A2); }
function tr     ($A1, $A2 = null) { return tag('tr', $A1, $A2); }
function td     ($A1, $A2 = null) { return tag('td', $A1, $A2); }
function h1     ($A1, $A2 = null) { return tag('h1', $A1, $A2); }
function h2     ($A1, $A2 = null) { return tag('h2', $A1, $A2); }
function h4     ($A1, $A2 = null) { return tag('h4', $A1, $A2); }
function h5     ($A1, $A2 = null) { return tag('h5', $A1, $A2); }
function h6     ($A1, $A2 = null) { return tag('h6', $A1, $A2); }
function ul     ($A1, $A2 = null) { return tag('ul', $A1, $A2); }
function li     ($A1, $A2 = null) { return tag('li', $A1, $A2); }
function div    ($A1, $A2 = null) { return tag('div', $A1, $A2); }
function img    ($A1, $A2 = null) { return tag('img', $A1, $A2); }
function pre    ($A1, $A2 = null) { return tag('pre', $A1, $A2); }
function span   ($A1, $A2 = null) { return tag('span', $A1, $A2); }
function strong ($A1, $A2 = null) { return tag('strong', $A1, $A2); }
function table  ($A1, $A2 = null) { return tag('table', $A1, $A2); }
function thead  ($A1, $A2 = null) { return tag('thead', $A1, $A2); }
function tbody  ($A1, $A2 = null) { return tag('tbody', $A1, $A2); }
function label  ($A1, $A2 = null) { return tag('label', $A1, $A2); }
function select ($A1, $A2 = null) { return tag('select', $A1, $A2); }
function option ($A1, $A2 = null) { return tag('option', $A1, $A2); }
function optgroup($A1, $A2 = null) { return tag('optgroup', $A1, $A2); }

function input  ($A1) {
    if (is_array($A1))
        $A1 = array_string(attributes($A1), ' ');
    return sprintf('<input %s>', $A1);
}
function radio($group, $value) { return input(array('type'=>"radio", 'name'=>$group, 'value'=>$value),array()); }

/*
 * name: btn,cmd
 *  utilities to generate wp_settings API visual fields
 */
function btn($action, $caption) {
    return div(array(
        input(id($action.'_btn') + array('type' => 'button', 'value' => $caption, 'class' => 'button')),
    ));
}
function cmd($cmd, $label, $desc = null) {
    return array( 'id' => $cmd, 'type' => 'content', 'content' => btn($cmd, $label), 'desc' => $desc );
}

function label_slug($Label) {
    return strtolower(str_replace(' ', '-', $Label));
}

/*
 * name: table_with_header
 * @param
 * @return: HTML
 */
function table_with_header(array $t_array, $ID) {
    $html[] = tr(array_map(function($k) { return th($k); }, array_keys((array)$t_array[0])));
    foreach ($t_array as $r)
        $html[] = array_map(function($e) { return td($e); }, $r);
    return table(id($ID), $html);
}

/*
 *
 * name: url
 * @param
 * @return
 *
 */
function url($resource) {
    $url = plugins_url($resource, __FILE__);
    return $url;
}

/*
 * name: css
 * @param css - note: css is relative to __FILE__ - so, don't pass initial slash
 * @return
 */
function css($k, $css) {
    if (substr($css, 0, 4) == 'http')
        wp_enqueue_style($k, $css.'.css');
    else
        wp_enqueue_style($k, url($css.'.css'));
}

/*
 * name: script
 * @param script: see above
 * @return
 */
function script($k, $script, $dep) {
    $req = $dep ? $dep : array();
    wp_enqueue_script($k, url($script), $req);
}

/*
 * name: js_
 * @param
 * @return
 */
function js_($k, $js, $dep) {
    script($k, $js.'.js', $dep);
}

/*
 * name: png
 * @param
 * @return
 */
function png($path) {
    return url($path.'.png');
}

/*
 * name: bulk_operation_optimize
 * @param
 * @return
 */
function bulk_operation_optimize($on_off) {
    wp_defer_term_counting($on_off);
    wp_defer_comment_counting($on_off);
    global $wpdb;
    if ($on_off) {
        $wpdb->query('set autocommit=0;');
    }
    else {
        $wpdb->query('commit;');
        $wpdb->query('set autocommit=1;');
    }
}

/*
 * name: inc_key
 * @param
 * @return
 */
function inc_key(array &$counters, $key, $val = 1) {
    if (isset($counters[$key]))
        $counters[$key] += $val;
    else
        $counters[$key] = $val;
}

/*
 * name: array_2_table
 * @param
 * @return
 */
function array_2_table(array $a) {
    return table(array_map_kv(function($k,$v) { return tr(array(td($k),td($v))); }, $a));
}

/*
 * name: scan_1
 * @param
 * @return
 */
function scan_1($val, $spec) {
    if ($val = sscanf($val, $spec))
        return $val[0];
}

function assign_if_not($type, $if_not, $not_overwrite) {
    return array_search($type, $not_overwrite) === false ? $if_not : $type;
}

/* delete from wp_postmeta where post_id not in (select ID from wp_posts) */
/* sudo sh -c ">/var/log/apache2/error.log" */
/* sudo service apache2 restart */

function rgb($color, $inner) { return div(style(array('color'=>$color)), $inner); }
function ok() { return rgb('green', 'ok'); }
function ko() { return rgb('red', 'ko'); }

function flag($ok) { return b($ok ? ok() : ko()); }
function ver($min) { return flag(phpversion() >= $min); }
function ext($ext) { return flag(extension_loaded($ext)); }
function has_gd()  { return flag(ext('GD') || (function_exists('gd_info') && is_array(gd_info()))); }

function capture_html($function, $args = null) {
    ob_start();
    call_user_func($function, $args);
    $html = ob_get_contents();
    ob_end_clean();
    return $html;
}
