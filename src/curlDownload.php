<?php

/**
 * Fattura24.com
 * Description: verbatim from fattura24 for Prestashop, reuse working code
 * Author: Ing. Carlo Capelli
 */

namespace fattura24;

if (!defined('ABSPATH'))
    exit;

function curlDownload($url, $data_string)
{
    if(!function_exists('curl_init'))
    { 
        trace('curl error: curl is not installed'); 
        die('Sorry, cURL is not installed!'); 
    } 
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    $config = array();
    $config['useragent'] = 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:17.0) Gecko/20100101 Firefox/17.0';
    curl_setopt($ch, CURLOPT_USERAGENT, $config['useragent']);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    if(!($output = curl_exec($ch)))
        trace('curl error', curl_getinfo($ch), curl_error($ch), curl_errno($ch));
    curl_close($ch);

    return $output;
}
