# [Plugin Fattura24 per Wordpress-WooCommmerce](http://www.fattura24.com/woocommerce-plugin-fatturazione/)

WooCommerce è diventato in pochissimo tempo il plugin WordPress di commercio elettronico più diffuso in Italia.
Completamente gratuito, è lo strumento ideale per chiunque voglia aprire velocemente un e-commerce professionale su WordPress senza avere particolari conoscenze tecniche.
Vediamo come estendere le funzionalità di fatturazione di WooCommerce integrandolo con il servizio di fatturazione di [Fattura24](http://www.fattura24.com/).

### step 1 – preparazione
Per utilizzare Fattura24 come servizio di fatturazione in WooCommerce ti occorre:
- un abbonamento Business attivo su Fattura24
- l’API KEY associata al tuo abbonamento Business
per ottenerla vai in ‘Configurazione’->’App e Servizi esterni’, clicca sul pulsante ‘Configura’ di WooCommerce e richiedi l’API KEY che ti sarà subito mostrata
- scaricare il plugin di Fattura24

### step 2 – configurazione
Di seguito i passaggi da seguire all’interno della console di amministrazione di WordPress
- da menu va in ‘Plugin’ -> ’Aggiungi nuovo’
- seleziona e carica il file zip scaricato in questa pagina
- clicca sul pulsante ‘Carica il modulo’
- nella sezione ‘Elenco moduli’ cercare il modulo ‘fattura24.com’
- clicca sul pulsante verde ‘Installa’ e quando ti verrà richiesto clicca su ‘Procedi con l’installazione’
- inserisci nel campo ‘Api Key’ la tua API KEY di Fattura24
- clicca su ‘Verifica API KEY’ per accertarti di aver inserito l’API KEY correttamente
- clicca su ‘Salva le modifiche’

### step 3 – verifica
La configurazione si può considerare terminata e possiamo procedere con un test.
Ogni qual volta riceverai un ordine sul tuo e-commerce, il modulo farà per conto tuo le seguenti operazioni:
- se hai spuntato 'Salva cliente', aggiungerà il cliente nella rubrica di Fattura24 o ne aggiornerà i dati se già presente
- se hai spuntato 'Crea ordine' nelle impostazioni, invierà a Fattura24 i dati dell'ordine
- se hai spuntato 'Invia email', verrà inviata un'email al cliente con il PDF dell'ordine

Quando imposti lo stato dell'ordine su ‘Completato’, il modulo farà per conto tuo le seguenti operazioni:
- se hai spuntato 'Crea fattura' nelle impostazioni, creerà in Fattura24 una ricevuta, se il cliente non ha una Partita IVA, una fattura altrimenti
- aggiungerà il cliente nella tua rubrica o ne aggiornerà i dati se già presente
- creerà il PDF della ricevuta/fattura
- se hai spuntato 'Scarica PDF' scaricherà una copia del PDF dentro il tuo e-commerce
- se hai spuntato 'Invia email', verrà inviata un'email al cliente con il PDF della ricevuta/fattura
- se hai spuntato 'Stato pagata', il documento creato in Fattura è già nello stato 'pagato'
- se hai spuntato 'Disabilita ricevute', verrà creata una fattura anziché una ricevuta anche in assenza della Partita IVA

È possibile creare una fattura anche cliccando sul pulsante ‘Crea Fattura’ nella tabella degli ordini. Allo stesso modo è possibile scaricare il PDF dentro il tuo e-commerce cliccando sul pulsante ‘Scarica PDF’.

Il modulo inoltre aggiungerà i campi ‘Codice Fiscale’ e ‘Partita IVA’ ai dati del cliente. Nelle impostazioni del modulo si può scegliere se il cliente sia obbligato o meno a valorizzare questi campi in fase di registrazione.

Come hai visto utilizzare Fattura24 con WooCommerce è semplicissimo ma se volessi assistenza tecnica non esitare a contattarci al numero +39 06-40402261.
Il nostro team tecnico è a tua completa disposizione per permetterti di ottenere il meglio dal tuo e-commerce e da Fattura24.